package main

import (
	"image"
	"image/color"
	"image/png"
	"log"
	"math/rand"
	"os"
	"time"

	"gitlab.com/alex.dogonin/rectangles-drawing/visualization/circle"
)

func main() {
	file, err := os.Create("someimage.png")
	if err != nil {
		log.Print(err)
		os.Exit(1)
	}
	lineColor := color.RGBA{0, 0, 0, 255}

	values := make([]float64, 250)
	rand.Seed(time.Now().Unix())
	for i := range values {
		values[i] = rand.Float64() + 0.1
	}

	canvas := image.NewRGBA(image.Rect(0, 0, 500, 400))

	circle.DrawSolidToImg(canvas, .7, .9, values, lineColor)

	if err = png.Encode(file, canvas); err != nil {
		log.Print(err)
		os.Exit(1)
	}
}

// type Circle struct {
// 	p image.Point
// 	r int
// }

// func (c *Circle) ColorModel() color.Model {
// 	return color.RGBA
// }

// func (c *Circle) Bounds() image.Rectangle {
// 	return image.Rect(c.p.X-c.r, c.p.Y-c.r, c.p.X+c.r, c.p.Y+c.r)
// }

// func (c *Circle) At(x, y int) color.Color {
// 	xx, yy, rr := float64(x-c.p.X)+0.5, float64(y-c.p.Y)+0.5, float64(c.r)
// 	if xx*xx+yy*yy < rr*rr {
// 		return color.Alpha{255}
// 	}
// 	return color.Alpha{0}
// }

// type Figure struct {
// 	height,
// 	width int
// 	angle float64
// }

// func NewRectangle(height, width int, angle float64) *Figure {
// 	return &Figure{
// 		height: height,
// 		width:  width,
// 		angle:  angle,
// 	}
// }

// func (r *Figure) ColorModel() color.Model {
// 	return color.AlphaModel
// }

// func (r *Figure) At(x, y int) color.Color {
// 	cos := math.Cos(r.angle)
// 	sin := math.Sin(r.angle)

// 	xx := int(float64(x)*cos - float64(y)*sin)
// 	yy := int(float64(x)*sin + float64(y)*cos)

// 	if xx < 0 || xx > r.width {
// 		return color.Alpha{0}
// 	}

// 	if yy < 0 || yy > r.height {
// 		return color.Alpha{0}
// 	}

// 	return color.Alpha{255}
// }

// func (r *Figure) Bounds() image.Rectangle {
// 	return image.Rect(
// 		0,
// 		0,
// 		int(float64(r.width)*math.Cos(r.angle)+float64(r.height)*math.Cos(math.Pi/4-r.angle)),
// 		int(float64(r.width)*math.Sin(math.Pi/4-r.angle)+float64(r.height)*math.Sin(r.angle)),
// 	)
// }
