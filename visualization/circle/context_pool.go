package circle

import (
	"github.com/fogleman/gg"
	"sync"
)

type contextPool struct {
	pools map[int]map[int]*sync.Pool
}

func newContextPool() *contextPool {
	return &contextPool{
		make(map[int]map[int]*sync.Pool),
	}
}

func (p *contextPool) Get(width, height int) *gg.Context {
	poolsByHeight, ok := p.pools[width]
	if !ok {
		return gg.NewContext(width, height)
	}

	pool, ok := poolsByHeight[height]
	if !ok {
		return gg.NewContext(width, height)
	}

	ctx := pool.Get()
	if ctx == nil {
		return gg.NewContext(width, height)
	}

	return ctx.(*gg.Context)
}

func (p *contextPool) Put(ctx *gg.Context) {
	poolsByHeight, ok := p.pools[ctx.Width()]
	if !ok {
		poolsByHeight = make(map[int]*sync.Pool)
		p.pools[ctx.Width()] = poolsByHeight
	}

	pool, ok := poolsByHeight[ctx.Height()]
	if !ok {
		pool = &sync.Pool{}
		poolsByHeight[ctx.Height()] = pool
	}

	ctx.Clear()
	pool.Put(ctx)
}
