package circle

import (
	"image"
	"math"

	"image/color"

	"image/draw"
)

var dcPool = newContextPool()

func DrawSolidToImg(dst draw.Image, radius, lineWidth float64, values []float64, color color.Color) {
	dc := dcPool.Get(dst.Bounds().Dx(), dst.Bounds().Dy())
	defer dcPool.Put(dc)

	centerX := float64(dst.Bounds().Dx()) / 2
	centerY := float64(dst.Bounds().Dy()) / 2
	step := (2. * math.Pi) / float64(len(values))

	factWidth := math.Min(float64(dst.Bounds().Dx()), float64(dst.Bounds().Dy()))
	absRadius := factWidth / 2 * radius
	lineHeight := factWidth/2 - absRadius
	absLineWidth := 2 * math.Pi * absRadius * lineWidth / float64(len(values))

	dc.SetColor(color)
	for _, v := range values {
		dc.RotateAbout(step, centerX, centerY)

		dc.DrawRectangle(centerX-absLineWidth/2, centerY+absRadius, absLineWidth, lineHeight*v)
	}
	dc.Fill()

	draw.Draw(dst, dst.Bounds(), dc.Image(), image.ZP, draw.Over)
}
